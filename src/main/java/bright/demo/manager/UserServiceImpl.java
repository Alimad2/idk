package bright.demo.manager;

import bright.demo.domain.dto.AuthUserDetail;
import bright.demo.domain.dto.UserDto;
import bright.demo.domain.entity.*;
import bright.demo.repository.PasswordTokenRepository;
import bright.demo.repository.RoleRepository;
import bright.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class UserServiceImpl implements UserDetailsService {

//    @Autowired
//    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordTokenRepository passwordTokenRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> optionalUser = userRepository.findByUsername(username);
        User user = optionalUser.orElseThrow(() -> new UsernameNotFoundException("username or password is wrong!"));

        UserDetails userDetails = new AuthUserDetail(user);

        new AccountStatusUserDetailsChecker().check(userDetails);

        return userDetails;
    }

    public void registerUser(UserDto userDto) {
        User user = new Client();
        user.setUsername(userDto.getUsername());
        user.setPassword(new BCryptPasswordEncoder().encode(userDto.getPassword()));
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setPhoneNumber(userDto.getPhoneNumber());
        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);
        user.setEnabled(true);
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(roleRepository.findById(3).get());
        user.setRoles(roles);
        userRepository.save(user);
    }

    public User loadUserById(int cid) {
        Optional<User> optionalUser = userRepository.findById(cid);
        User user = optionalUser.orElseThrow(() -> new UsernameNotFoundException("username or password is wrong!"));
        return user;
    }

    public User laodUserByUsername(String username) {
        Optional<User> optionalUser = userRepository.findByUsername(username);
        User user = optionalUser.orElseThrow(() -> new UsernameNotFoundException("error!"));
        return user;
    }

    public User loadUserByEmail(String email) {
        Optional<User> optionalUser = userRepository.findByEmail(email);
        User user = optionalUser.orElseThrow(() -> new UsernameNotFoundException("email is wrong!"));
        return user;
    }

    public void createPasswordResetTokenForUser(User user, String token) {
        PasswordResetToken myToken = new PasswordResetToken(user, token);
        passwordTokenRepository.save(myToken);
    }

    public void changePassword(User user, String pass) {
        System.out.println("password is : " + pass);
        user.setPassword(new BCryptPasswordEncoder().encode(pass));
        userRepository.save(user);
    }


}
