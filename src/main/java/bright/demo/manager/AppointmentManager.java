package bright.demo.manager;

import bright.demo.domain.dto.apt.AppointmentAttachDto;
import bright.demo.domain.dto.apt.AppointmentReport;
import bright.demo.domain.entity.*;
import bright.demo.domain.exception.BaseException;
import bright.demo.repository.AppointmentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@Slf4j
public class AppointmentManager {

    @Autowired
    private AppointmentRepository appointmentRepository;

    @Autowired
    private UserServiceImpl userService;

    public Appointment loadAppointmentByUsername(String username) {
        User user = userService.laodUserByUsername(username);
        int uid = user.getId();
        return loadAppointmentByClientId(uid);
    }

    private Appointment loadAppointmentByClientId(Integer uid) {
        Optional<Appointment> optionalAppointment = appointmentRepository.findByClient_Id(uid);
        return optionalAppointment.get();
    }

    public List<Appointment> loadAppointmentsByPid(int pid) throws BaseException {
        User user = userService.loadUserById(pid);
        if (!(user instanceof Photographer)) {
            throw new BaseException("id is wrong");
        }
        return appointmentRepository.findByPhotographer_Id(pid);
    }

    public boolean updateAppointment(int aid, String time, String username, Integer pid) {
        try {
            User user = userService.laodUserByUsername(username);
            int cid = user.getId();
            Appointment appointmentToUpdate = appointmentRepository.getOne(aid);
            if (appointmentToUpdate.getClient().getId() != cid) {
                throw new BaseException("can't access!");
            }
            appointmentToUpdate.setTime(time);
            appointmentToUpdate.setClient((Client) userService.loadUserById(cid));
            appointmentToUpdate.setPhotographer((Photographer) userService.loadUserById(pid));
            appointmentRepository.save(appointmentToUpdate);
        } catch (Exception e) {
            log.error(e.getMessage() + ", appointment number " + aid);
            return false;
        }
        return true;

    }

    public boolean attachAppointment(String username, AppointmentAttachDto appointmentDto) {
        Integer aid = appointmentDto.getId();
        Photographer photographer = (Photographer) userService.loadUserById(appointmentDto.getPid());
        Client client = (Client) userService.laodUserByUsername(username);
        Appointment appointment = appointmentRepository.getOne(aid);
        if (appointment.getClient() != null && appointment.getPhotographer() == null) {
            log.error("client is not null or photographer is null");
            return false;
        }
        appointment.setPhotographer(photographer);
        appointment.setClient(client);
        appointmentRepository.save(appointment);
        return true;
    }

    public boolean detachAppointment(String username) {
        try {
            Client client = (Client) userService.laodUserByUsername(username);
            Appointment appointment = loadAppointmentByClientId(client.getId());
            Appointment apt = appointmentRepository.getOne(appointment.getId());
            apt.setClient(null);
            apt.setPhotographer(null);
            appointmentRepository.save(apt);
            return true;
        } catch (Exception e) {
            log.error(e.getMessage() + " , detach, username : " + username);
            return false;
        }
    }

    public List<AppointmentReport> getFreeAppointments() {
        List<Appointment> appointments = appointmentRepository.findAllByClientOrderByTime(null);
        return appointmentToReport(appointments);
    }

    private List<AppointmentReport> appointmentToReport(List<Appointment> appointments) {
        List<AppointmentReport> appointmentReports = new ArrayList<>();
        for (int i = 0; i < appointments.size(); i++) {
            String time = appointments.get(i).getTime();
            int j = i;
            while (j < appointments.size() && time.equals(appointments.get(j).getTime())) {
                j++;
            }
            boolean flag = false;
            for (int o = i; o < j; o++) {
                if (!flag) {
                    appointmentReports.add(new AppointmentReport());
                    appointmentReports.get(appointmentReports.size() - 1).setTime(time);
                    appointmentReports.get(appointmentReports.size() - 1).setPhotographers(new ArrayList<>());
                    flag = true;
                }
                appointmentReports.get(appointmentReports.size() - 1).getPhotographers().add(appointments.get(o).getPhotographer());
            }
            i = j - 1;
        }
        return appointmentReports;
    }


}
