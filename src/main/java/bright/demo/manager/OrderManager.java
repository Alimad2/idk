package bright.demo.manager;

import bright.demo.domain.entity.Client;
import bright.demo.domain.entity.Order;
import bright.demo.domain.entity.SalesExpert;
import bright.demo.domain.exception.OrderNotFoundException;
import bright.demo.repository.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Slf4j
@Component
public class OrderManager {
    @Autowired
    OrderRepository orderRepository;

    @Autowired
    UserServiceImpl userService;

    public Order loadOrderById(int oid) throws OrderNotFoundException {
        Optional<Order> optionalOrder = orderRepository.findById(oid);
        Order order = optionalOrder.orElseThrow(() -> new OrderNotFoundException("id is wrong"));
        return order;
    }

    public boolean createOrder(int cid, int sid) {
        try {
            Order order = new Order();
            Client client = (Client) userService.loadUserById(cid);
            SalesExpert salesExpert = (SalesExpert) userService.loadUserById(sid);
            order.setClient(client);
            order.setSalesExpert(salesExpert);
            orderRepository.save(order);
        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean updateOrder(Integer oid, Integer cid, Integer sid) {
        try {
            Order orderToUpdate = orderRepository.getOne(oid);
            System.out.println("id is : " + oid);
            Client client = (Client) userService.loadUserById(cid);
            SalesExpert salesExpert = (SalesExpert) userService.loadUserById(sid);
            orderToUpdate.setClient(client);
            orderToUpdate.setSalesExpert(salesExpert);
            orderRepository.save(orderToUpdate);
        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
        return true;
    }
}
