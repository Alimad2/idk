package bright.demo.repository;

import bright.demo.domain.entity.Appointment;
import bright.demo.domain.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Transactional
@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Integer> {

    Optional<Appointment> findByClient_Id(Integer cid);

    List<Appointment> findByPhotographer_Id(Integer pid);

    List<Appointment> findAllByClientOrderByTime(Client client);
}
