package bright.demo.controller;

import bright.demo.domain.dto.OrderDto;
import bright.demo.domain.entity.Order;
import bright.demo.domain.exception.OrderNotFoundException;
import bright.demo.manager.OrderManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/order")
@Slf4j
public class OrderController {

    @Autowired
    OrderManager orderManager;

    @GetMapping("/{oid}")
    @Secured({"ROLE_SALESEXPERT"})
    @ResponseBody
    public String getOrder(@PathVariable("oid") int oid) {
        try {
            Order order = orderManager.loadOrderById(oid);
            return order.toString();
        } catch (OrderNotFoundException e) {
            log.error(e.getMessage());
        }
        return "";
    }

    @PutMapping("/{oid}")
    @Secured({"ROLE_SALESEXPERT"})
    @ResponseBody
    public String updateOrder(@PathVariable("oid") Integer oid, @RequestBody @Valid  OrderDto orderDto) {
        boolean flag = orderManager.updateOrder(oid, orderDto.getCid(), orderDto.getSid());
        if (flag) {
            log.info("the order has been updated");
        } else {
            log.error("an error occurred during order update");
        }
        return flag ? "updated" : "error";

    }

    @PostMapping("/create")
    @Secured({"ROLE_SALESEXPERT"})
    @ResponseBody
    public String createOrder(@RequestBody OrderDto orderDto) {
        boolean flag = orderManager.createOrder(orderDto.getCid(), orderDto.getSid());
        if (flag) {
            log.info("the order has been created");
        } else {
            log.error("an error occurred during order creation");
        }
        return flag ? "created" : "error";

    }
}
