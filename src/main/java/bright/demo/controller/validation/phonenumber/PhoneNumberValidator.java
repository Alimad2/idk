package bright.demo.controller.validation.phonenumber;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneNumberValidator implements ConstraintValidator<ValidPhoneNumber, String> {

    private Pattern pattern;
    private Matcher matcher;
    private static final String PHONENUMBER_PATTERN = "((?=.*\\d).{11})";

    @Override
    public boolean isValid(String number, ConstraintValidatorContext constraintValidatorContext) {
        return validatePhone(number);
    }

    private boolean validatePhone(String number) {
        pattern = Pattern.compile(PHONENUMBER_PATTERN);
        matcher = pattern.matcher(number);
        return matcher.matches();
    }
}
