package bright.demo.controller;

import bright.demo.controller.validation.UserValidator;
import bright.demo.domain.dto.UserDto;
import bright.demo.domain.entity.Appointment;
import bright.demo.domain.entity.Client;
import bright.demo.domain.entity.User;
import bright.demo.domain.exception.BaseException;
import bright.demo.manager.AppointmentManager;
import bright.demo.manager.EmailService;
import bright.demo.manager.UserServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/")
@Slf4j
public class MainController {

    @Autowired
    private AppointmentManager appointmentManager;

    @Autowired
    private UserServiceImpl userService;


    @GetMapping("/register")
    @ResponseBody
    public String showRegistrationForm() {
        return "this is sign up";
    }

    @PostMapping("/register")
    @ResponseBody
    public String registerConfirmation(@RequestBody @Valid UserDto userDto) {
        try {
            userService.registerUser(userDto);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        log.info("user has been successfully registered!");
        return "welcome";
    }

    @GetMapping("/{pid}/apts")//pid = photographerId
    public String getAppointmentsOfPhotographer(@PathVariable("pid") int pid) {
        List<Appointment> appointments = null;
        try {
            appointments = appointmentManager.loadAppointmentsByPid(pid);
        } catch (BaseException e) {
            log.error(e.getMessage());
        }
        return appointments.toString();
    }


    @GetMapping("/client/{cid}")
    @ResponseBody
    public String getClient(@PathVariable("cid") int cid) {
        User user = userService.loadUserById(cid);
        if (user.getClass().equals(Client.class)) {
            return user.toString();
        }
        log.error("id is wrong, or user is not a client");
        return "id is wrong";
    }

    @PostMapping("/ticket/sales/apt")
    @Secured({"ROLE_SALESEXPERT"})
    public String requestAppointmentSet() {
        return "";
    }

    @GetMapping("/ticket/sales/apt")
    @Secured({"ROLE_SALESEXPERT"})
    public String getAppointmentSetRequests() {
        return "";
    }


}
