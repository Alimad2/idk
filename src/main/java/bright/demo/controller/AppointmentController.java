package bright.demo.controller;

import bright.demo.config.IAuthenticationFacade;
import bright.demo.domain.dto.apt.AppointmentAttachDto;
import bright.demo.domain.dto.apt.AppointmentDto;
import bright.demo.domain.dto.apt.AppointmentReport;
import bright.demo.domain.entity.Appointment;
import bright.demo.manager.AppointmentManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/apt")
@Slf4j
public class AppointmentController {

    @Autowired
    private AppointmentManager appointmentManager;

    @Autowired
    private IAuthenticationFacade authenticationFacade;

    @GetMapping("")
    @ResponseBody
    public String findAppointment() {
        Authentication authentication = authenticationFacade.getAuthentication();
        String username = authentication.getName();
        Appointment appointment = appointmentManager.loadAppointmentByUsername(username);
        return appointment.toString();
    }

    @PostMapping("/detach")
    @ResponseBody
    public String detachAppointment() {
        Authentication authentication = authenticationFacade.getAuthentication();
        String username = authentication.getName();
        boolean flag = appointmentManager.detachAppointment(username);
        if (flag) {
            log.info("detached " + username + " from his/her appointment");
        } else {
            log.error("error occurred during detach, username is : " + username);
        }
        return flag ? "detached" : "error";
    }

    @PostMapping("/attach")
    @ResponseBody
    public String attachAppointment(@RequestBody @Valid AppointmentAttachDto appointmentAttachDto) {
        Authentication authentication = authenticationFacade.getAuthentication();
        String username = authentication.getName();
        boolean flag = appointmentManager.attachAppointment(username, appointmentAttachDto);
        if (flag) {
            log.info("attached " + username + " to appointment number " + appointmentAttachDto.getId());
        } else {
            log.error("error during attachment, username is : " + username + " appointment id is " + appointmentAttachDto.getId());
        }
        return flag ? "attached" : "error";
    }

    @PutMapping("/{aid}")
    @ResponseBody
    public String updateAppointment(@PathVariable("aid") int aid, @RequestBody @Valid AppointmentDto appointmentDto) {
        Authentication authentication = authenticationFacade.getAuthentication();
        String username = authentication.getName();
        boolean flag = appointmentManager.updateAppointment(aid, appointmentDto.getTime(), username, appointmentDto.getPid());
        if (flag) {
            log.info("appointment number " + aid + " has been updated");
        } else {
            log.error("error occurred during update of appointment number " + aid);
        }
        return flag ? "updated" : "error";
    }

    @GetMapping("/report")
    @ResponseBody
    public String reportAppointment() {
        List<AppointmentReport> reports = appointmentManager.getFreeAppointments();
        return reports.toString();
    }
}
