package bright.demo.controller;

import bright.demo.controller.validation.UserValidator;
import bright.demo.domain.dto.EmailDto;
import bright.demo.domain.dto.PasswordDto;
import bright.demo.domain.entity.User;
import bright.demo.manager.EmailService;
import bright.demo.manager.UserServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/forget")
@Slf4j
public class ForgetPasswordController {

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private EmailService emailService;

    @GetMapping("")
    @ResponseBody
    public String forgetPassword() {
        return "this is forget";
    }

    @PostMapping("")
    @ResponseBody
    public String checkEmail(@RequestBody @Valid EmailDto emailDto) {
        User user = userService.loadUserByEmail(emailDto.getEmail());
        if (user == null) {
            log.error("there is no user with this email " + emailDto.getEmail());
            return "error";
        }
        String token = UUID.randomUUID().toString();
        userService.createPasswordResetTokenForUser(user, token);
        String url = "localhost:8080/changePassword?id=" + user.getId() + "&token=" + token;
        emailService.sendSimpleMessage(emailDto.getEmail(), "IDK::[Reset Password]",
                "click the link below to reset your password : \n" + url);
        log.info("a reset password link has been sent to username " + user.getUsername() + "'s email");
        return "sent";
    }

    @GetMapping("/changePassword")
    @ResponseBody
    public String showChangePasswordPage(@RequestParam("id") Integer id, @RequestParam("token") String token) {// id = user's id
        String result = userValidator.validatePasswordResetToken(id, token);
        if (result != null) {
            log.error("error during reset token validation");
            return "error";
        }
        return "done";//redirect to updatePassword page
    }

    @PostMapping("/updatePassword")
    @ResponseBody
    public String updateNewPassword(@RequestBody @Valid PasswordDto passwordDto) {
        User user =
                (User) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal();
        userService.changePassword(user, passwordDto.getPassword());
        log.info("user " + user.getUsername() + "'s password has been changed");
        return "changed!";
    }
}
