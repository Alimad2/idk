package bright.demo.domain.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class OrderDto {

    @NotNull
    @NotEmpty
    private Integer cid;//client id

    @NotNull
    @NotEmpty
    private Integer sid;//sales expert id
}
