package bright.demo.domain.dto;

import bright.demo.controller.validation.email.ValidEmail;
import bright.demo.controller.validation.password.ValidPassword;
import bright.demo.controller.validation.phonenumber.ValidPhoneNumber;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class UserDto {

    @NotNull
    @NotEmpty
    private String username;

    @NotNull
    @NotEmpty
    @ValidPassword
    private String password;

    @NotNull
    @NotEmpty
    @ValidEmail
    private String email;

    @NotNull
    @NotEmpty
    private String firstName;


    private String lastName;

    @NotNull
    @NotEmpty
    @ValidPhoneNumber
    private String phoneNumber;

}
