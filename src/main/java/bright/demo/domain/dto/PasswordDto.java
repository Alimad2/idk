package bright.demo.domain.dto;

import bright.demo.controller.validation.password.ValidPassword;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class PasswordDto {

    @NotNull
    @NotEmpty
    @ValidPassword
    private String password;
}
