package bright.demo.domain.dto;

import bright.demo.controller.validation.email.ValidEmail;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class EmailDto {

    @NotNull
    @NotEmpty
    @ValidEmail
    private String email;
}
