package bright.demo.domain.dto.apt;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class AppointmentAttachDto {

    @NotNull
    @NotEmpty
    private Integer id;//appointment id

    @NotNull
    @NotEmpty
    private Integer pid;//photographer id
}
