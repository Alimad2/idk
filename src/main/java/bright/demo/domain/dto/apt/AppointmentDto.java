package bright.demo.domain.dto.apt;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class AppointmentDto {

    @NotNull
    @NotEmpty
    private String time;

    @NotNull
    @NotEmpty
    private Integer pid;//photographer id
}
