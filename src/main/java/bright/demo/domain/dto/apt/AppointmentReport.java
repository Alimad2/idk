package bright.demo.domain.dto.apt;

import bright.demo.domain.entity.Photographer;
import lombok.Data;

import java.util.List;

@Data
public class AppointmentReport {

    private String time;

    private List<Photographer> photographers;

}
