package bright.demo.domain.exception;

public class NonNumericalException extends BaseException {

    public NonNumericalException() {

        super("parameter should be numerical");
    }
}
