package bright.demo.domain.exception;

public class BaseException extends Exception {

    public BaseException(String message) {
        super(message);
    }
}
