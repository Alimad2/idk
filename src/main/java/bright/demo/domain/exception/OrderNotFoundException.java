package bright.demo.domain.exception;

public class OrderNotFoundException extends BaseException {

    public OrderNotFoundException(String message) {
        super(message);
    }
}
