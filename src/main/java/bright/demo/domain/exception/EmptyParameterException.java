package bright.demo.domain.exception;

public class EmptyParameterException extends BaseException {
    public EmptyParameterException() {

        super("parameter shouldn't be empty");
    }
}
