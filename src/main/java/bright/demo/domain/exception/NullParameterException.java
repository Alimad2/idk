package bright.demo.domain.exception;

public class NullParameterException extends BaseException {

    public NullParameterException() {
        super("parameter shouldn't be null");
    }
}
