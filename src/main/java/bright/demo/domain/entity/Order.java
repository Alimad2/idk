package bright.demo.domain.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "orders")
@Data
@ToString
public class Order extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToMany(mappedBy = "order")
    private List<Photo> photos;

    @OneToOne
    @JoinColumn
    private Client client;

    @ManyToOne
    @JoinColumn
    private SalesExpert salesExpert;

    public String toString(){
        return "Order [id = " + getId()
                + ", client = " + getClient()
                + ", salesExpert = " + getSalesExpert()
                + " ]";
    }

}
