package bright.demo.domain.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Photo extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne
    @JoinColumn
    private Client client;

    @ManyToOne
    private Photographer photographer;

    @ManyToOne
    @JoinColumn
    private Order order;

    private String downloadLink;




}
