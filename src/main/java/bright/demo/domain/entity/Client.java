package bright.demo.domain.entity;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
@Data
public class Client extends User {
    public Client() {
    }

    @OneToOne(mappedBy = "client")
    private Order order;


    @OneToOne(mappedBy = "client")
    private Appointment appointment;


    public Client(User user) {
        super(user);
    }

    public String toString(){
        return "Client [username=" + getUsername() + ", first-name=" + getFirstName() +
                ", last-name=" + getLastName() + ", email=" + getEmail() + ", phone-number=" + getPhoneNumber() + "]";
    }
}
