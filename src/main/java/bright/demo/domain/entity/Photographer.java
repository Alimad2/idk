package bright.demo.domain.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@Data
public class Photographer extends User {
    public Photographer() {
    }

    @OneToMany(mappedBy = "photographer")
    private List<Appointment> appointments;

    public Photographer(User user) {
        super(user);
    }

    public String toString(){
        return "Photographer [id = " + getId() + "]";
    }
}
