package bright.demo.domain.entity;

public enum UserType {
    CLIENT,
    PHOTOGRAPHER,
    ADMIN,
    SALESEXPERT

}
