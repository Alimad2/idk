package bright.demo.domain.entity;

import javax.persistence.Entity;

@Entity
public class Admin extends User {

    public Admin() {
    }

    public Admin(User user) {
        super(user);
    }

}
