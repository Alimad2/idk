package bright.demo.domain.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Appointment extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToOne
    @JoinColumn
    private Client client;

    private String time;

    @ManyToOne
    @JoinColumn
    private Photographer photographer;

    public String toString(){
        return "Appointment[id = " + getId() + ", client = " + getClient()
                + ", time = " + getTime() + ", photographer = " + getPhotographer() +"]";
    }
}
