package bright.demo.domain.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;


@Data
@Entity
public class SalesExpert extends User {

    public SalesExpert() {
    }

    public SalesExpert(List<Order> orders) {
        this.orders = orders;
    }

    public SalesExpert(User user) {
        super(user);
    }

    @OneToMany(mappedBy = "salesExpert")
    private List<Order> orders;

    public String toString(){
        return "SalesExpert [username = " + getUsername()
                + ", firstName = " + getFirstName()
                + ", phoneNumber = " + getPhoneNumber()
                + ", id = " + getId()
                +" ]";
    }
}
